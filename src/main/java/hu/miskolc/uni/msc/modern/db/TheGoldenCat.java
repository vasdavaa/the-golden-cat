package hu.miskolc.uni.msc.modern.db;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.bson.Document;
import org.json.JSONArray;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

import hu.miskolc.uni.msc.modern.db.utils.Constants;
import hu.miskolc.uni.msc.modern.db.utils.MongoDbUtilities;
import hu.miskolc.uni.msc.modern.db.utils.XmlUtilities;

/**
 * Mongodb-t kezelő java program.
 * Létrehozható vele egy mongodb macskás adatbázis, amihez egy xml bemenetet
 * használ.
 * Lekérdezhetjük az aktuális adatainkat az adatbázisból.
 * Módosíthatjuk az adatokat az adatbázisban.
 * Törölhetünk adatokat az adatbázisból.
 * 
 * @author VassDavidAttila
 * @neptunCode F92356
 *
 */
public class TheGoldenCat {
	private TheGoldenCat() {}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		selectOperation(input);
	}

	/**
	 * Bekér a felhasználótól egy műveletet, majd végrehajta azt.
	 * 
	 * @param userInput Scanner a további felhasználói bemenetekhez.
	 */
	private static void selectOperation(Scanner userInput) {
		switch (userInputFromArray(userInput, Constants.OPERATION_MESSAGE,
			new String[] { Constants.CREATE_OPERATION, Constants.READ_OPERATION, Constants.UPDATE_OPERATION,
					Constants.DELETE_OPERATION })) {
		case Constants.CREATE_OPERATION:
			createDatabase(userInput);
			userInput.close();
			break;
		case Constants.READ_OPERATION:
			userInput.close();
			readDatabaseToXml();
			break;
		case Constants.UPDATE_OPERATION:
			updateDatabase(userInput);
			userInput.close();
			break;
		case Constants.DELETE_OPERATION:
			deleteCatFromDatabase(userInput);
			userInput.close();
			break;
		}
	}

	/**
	 * Módosíthatjuk egy macska adattagját vagy egy egész macska összes adattagját
	 * az adatbázisban.
	 * 
	 * @param userInput
	 */
	private static void updateDatabase(Scanner userInput) {

		MongoCollection<Document> collection = MongoDbUtilities.getCollection();
		int numberOfDocumentsInCollection = (int) collection.countDocuments();
		String updateOption = userInputFromArray(userInput, Constants.UPDATE_OPERATION_MESSAGE,
			new String[] { Constants.CAT, Constants.CAT_FIELD });

		// print cats out
		MongoDbUtilities.printCollection(collection);
		int catIdToBeUpdated = userInputBetweenRange(userInput,
			Constants.SELECT_CAT_ID_MESSAGE
				+ "\nAvailable cat ids range:[0.." + (numberOfDocumentsInCollection - 1)
				+ "]: ",
			numberOfDocumentsInCollection);

		// update a cat field
		if (updateOption.equals(Constants.CAT_FIELD)) {
			String catFieldToBeUpdated = userInputFromArray(userInput, Constants.SELECT_CAT_FIELD_MESSAGE,
				new String[] { Constants.CAT_NAME_ELEMENT, Constants.CAT_DATE_OF_BIRTH_ELEMENT,
						Constants.CAT_GENDER_ELEMENT, Constants.CAT_FOOD_ELEMENT });
			System.out.print(catFieldToBeUpdated + "=");
			String newValue = userInput.nextLine();
			MongoDbUtilities.updateDocumentField(Filters.eq(Constants.MONGODB_ID,
				MongoDbUtilities.getDocumentIdFromCollection(collection, catIdToBeUpdated)), catFieldToBeUpdated, newValue, collection);
		}
		// update a whole cat [name, dateborn, gender, favorite_food]
		if (updateOption.equals(Constants.CAT)) {
			System.out.print(Constants.CAT_NAME_ELEMENT + "=");
			String name = userInput.nextLine();
			System.out.print(Constants.CAT_DATE_OF_BIRTH_ELEMENT + "=");
			String dateborn = userInput.nextLine();
			System.out.print(Constants.CAT_GENDER_ELEMENT + "=");
			String gender = userInput.nextLine();
			System.out.print(Constants.CAT_FOOD_ELEMENT + "=");
			String favoriteFood = userInput.nextLine();
			MongoDbUtilities.updateDocumentFields(Filters.eq(Constants.MONGODB_ID,
				MongoDbUtilities.getDocumentIdFromCollection(collection, catIdToBeUpdated)),
				new String[] { Constants.CAT_NAME_ELEMENT, Constants.CAT_DATE_OF_BIRTH_ELEMENT, Constants.CAT_GENDER_ELEMENT,
						Constants.CAT_FOOD_ELEMENT },
				new String[] { name, dateborn, gender, favoriteFood }, collection);
		}
	}

	/**
	 * Három féle módon végezhető el a törlés művelet.
	 * 1. Mindent kitörlünk az adott kollekcióból.
	 * 2. Kitörlünk egy dokumentumot a kollekcióból.
	 * 3. Kitöröljük a dokumentum egy adattagját.
	 * 
	 * @param userInput Scanner a további felhasználói bemenetekhez.
	 */
	private static void deleteCatFromDatabase(Scanner userInput) {
		MongoCollection<Document> collection = MongoDbUtilities.getCollection();
		int numberOfDocumentsInCollection = (int) collection.countDocuments();
		String deleteOption = userInputFromArray(userInput, Constants.DELETE_OPERATION_MESSAGE,
			new String[] { Constants.CAT, Constants.CAT_FIELD, Constants.ALL_CATS });

		// delete all cats
		if (deleteOption.equals(Constants.ALL_CATS)) {
			if (numberOfDocumentsInCollection == 0) {
				System.out.println(Constants.NO_CATS_IN_COLLECTION_MESSAGE);
				return;
			}
			MongoDbUtilities.deleteCollectionFromDatabase(collection);
			return;
		}

		// print cats out
		MongoDbUtilities.printCollection(collection);
		int catIdToBeDeleted = userInputBetweenRange(userInput,
			Constants.SELECT_CAT_ID_MESSAGE
				+ "\nAvailable cat ids range:[0.." + (numberOfDocumentsInCollection - 1)
				+ "]: ",
			numberOfDocumentsInCollection);

		// delete a cat
		if (deleteOption.equals(Constants.CAT)) {
			MongoDbUtilities.deleteObjFromCollection(
				Filters.eq(Constants.MONGODB_ID,
					MongoDbUtilities.getDocumentIdFromCollection(collection, catIdToBeDeleted)),
				collection);
			return;
		}

		// delete a field of a cat
		if (deleteOption.equals(Constants.CAT_FIELD)) {
			String catFieldToBeDeleted = userInputFromArray(userInput, Constants.SELECT_CAT_FIELD_MESSAGE,
				new String[] { Constants.CAT_NAME_ELEMENT, Constants.CAT_DATE_OF_BIRTH_ELEMENT,
						Constants.CAT_GENDER_ELEMENT, Constants.CAT_FOOD_ELEMENT });
			MongoDbUtilities.deleteDocumentField(Filters.eq(Constants.MONGODB_ID,
				MongoDbUtilities.getDocumentIdFromCollection(collection, catIdToBeDeleted)), catFieldToBeDeleted,
				collection);
		}
	}

	/**
	 * Az adatbázisban tárolt adatokból egy xml fájlt készít "database.xml" néven a
	 * projekt jegyzékbe. (nevezhető letöltésnek is)
	 */
	private static void readDatabaseToXml() {
		MongoCollection<Document> collection = MongoDbUtilities.getCollection();
		XmlUtilities.writeCollectionToXml(collection);
	}

	/**
	 * Létrehozza az adatbázist és a kollekciót. Feldolgozza az xml fájlt(Dom),
	 * amiből json-t készít és végül beszúrja az adatbázisba.
	 * 
	 * @param userInput Scanner a további felhasználói bemenetekhez.
	 */
	private static void createDatabase(Scanner userInput) {
		System.out.println(Constants.FILENAME_MESSAGE);
		String filename = userInput.nextLine();
		File file = new File(filename);
		if (file.exists() && !file.isDirectory()) {
			JSONArray jArray = XmlUtilities.getJsonFromXML(filename);
			MongoCollection<Document> collection = MongoDbUtilities.getCollection();
			for (int i = 0; i < jArray.length(); i++) {
				MongoDbUtilities.insertJsonIntoCollection(collection, jArray.getJSONObject(i).toString());
			}
		} else {
			System.out.println("File does not exist.");
		}
	}

	/**
	 * Ellenörzött String bekérés, ami csak a megengedett bemenetet fogadja el.
	 * 
	 * @param userInput Scanner a további felhasználói bemenetekhez.
	 * @param message   az üzenet amit minden rossz bemenet esetén újra kiírok a
	 *                  console-ra.
	 * @param texts     elfogadott szövegek
	 * @return validált bemenet a felhasználótól
	 */
	private static String userInputFromArray(Scanner userInput, String message, String... texts) {
		Pattern patterWithAcceptedTexts = Pattern.compile(createPattern(texts));
		Matcher matcher;
		String validatedText = null;
		do {
			System.out.print(message);
			matcher = patterWithAcceptedTexts.matcher(validatedText = userInput.nextLine());
		} while (!(matcher.find()));

		return validatedText;
	}

	/**
	 * Visszaad egy reguláris kifejezést amit a megadott szövegek alapján
	 * készítettem.
	 * 
	 * @param texts adott szövegek a reguláris kifejezéshez
	 * @return az elkészült reguláris kifejezés
	 */
	private static String createPattern(String... texts) {
		return Arrays.asList(texts).stream().map(text -> ("^" + text + "$"))
			.collect(Collectors.joining("|"));
	}

	/**
	 * Ellenörzött egész szám bekérés, ami csak a megengedett intervallumon belül
	 * fogadja el a számokat.
	 * 
	 * @param userInput Scanner a további felhasználói bemenetekhez.
	 * @param message   az üzenet amit minden rossz bemenet esetén újra kiírok a
	 *                  console-ra.
	 * @param max       0 és (max-1) érték között fogadok el számokat.
	 * @return validált bemenet a felhasználótól
	 */
	private static int userInputBetweenRange(Scanner userInput, String message, int max) {
		Integer validatedNumber = null;
		do {
			System.out.print(message);
			try {
				validatedNumber = Integer.valueOf(userInput.nextLine());
			} catch (NumberFormatException e) {
				validatedNumber = -1;
				continue;
			}
		} while (!(validatedNumber >= 0 && validatedNumber < max));

		return validatedNumber;
	}
}
