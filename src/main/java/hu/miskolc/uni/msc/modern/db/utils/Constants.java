package hu.miskolc.uni.msc.modern.db.utils;

/**
 * Konstans oszt�ly
 * 
 * @author VassDavidAttila
 * @neptunCode F92356
 */
public class Constants {
	private Constants() {}

	// Messages
	public static final String OPERATION_MESSAGE = "Select an operation.\r\n"
		+ "create - Create database with an xml file\r\n"
		+ "read -  Read data from database\r\n"
		+ "update - Update data in database\r\n"
		+ "delete - Delete data from database\r\n"
		+ "Please select the operation by typing one of these:\r\n"
		+ "[create,read,update,delete]:";

	public static final String UPDATE_OPERATION_MESSAGE = "\nWould you like to update a cat or a field of a cat?\r\n"
		+ "cat - Update a cat document from the database\r\n"
		+ "field - Update a cat field from the database\r\n"
		+ "Please select the operation by typing one of these:\r\n"
		+ "[cat,field]:";

	public static final String DELETE_OPERATION_MESSAGE = "\nWould you like to delete a cat, a field of a cat or all the cats from the database?\r\n"
		+ "cat - Delete a cat document from the database\r\n"
		+ "field - Delete a cat field from the database\r\n"
		+ "all - Delete every cats from the database\r\n"
		+ "Please select the operation by typing one of these:\r\n"
		+ "[cat,field,all]:";

	public static final String FILENAME_MESSAGE = "[Xml file needs to be in the project directory]\r\n"
		+ "Please enter the xml file name with extension.";

	public static final String NO_CATS_IN_COLLECTION_MESSAGE = "There are no cats in the collection\n"
		+ "Exiting...";

	public static final String SELECT_CAT_ID_MESSAGE = "\nPlease select a cat id by typing it";
	public static final String SELECT_CAT_FIELD_MESSAGE = "\nPlease select a field by typing its name\n"
		+ "[name,dateborn,gender,favorite_food]: ";

	// Constants
	public static final String CAT = "cat";
	public static final String CAT_FIELD = "field";
	public static final String ALL_CATS = "all";

	public static final String CREATE_OPERATION = "create";
	public static final String READ_OPERATION = "read";
	public static final String UPDATE_OPERATION = "update";
	public static final String DELETE_OPERATION = "delete";

	// Mongodb related
	public static final String DATABASE_NAME = "TheGoldenCat";
	public static final String COLLECTION_NAME = "cats";
	public static final String MONGODB_ID = "_id";

	// Xml related
	public static final String CAT_ELEMENT = "cat";
	public static final String CAT_NAME_ELEMENT = "name";
	public static final String CAT_DATE_OF_BIRTH_ELEMENT = "dateborn";
	public static final String CAT_GENDER_ELEMENT = "gender";
	public static final String CAT_FOOD_ELEMENT = "favorite_food";
}
