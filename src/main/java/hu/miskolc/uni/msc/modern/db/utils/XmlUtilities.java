package hu.miskolc.uni.msc.modern.db.utils;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.mongodb.client.MongoCollection;

/**
 * Xml-hez kapcsolódó hasznos függvények könyvtára.
 * 
 * @author VassDavidAttila
 * @neptunCode F92356
 */
public class XmlUtilities {

	private XmlUtilities() {}

	/**
	 * Visszaad egy JSONArray-t amit az xmlben talált adatokból állít elő.
	 * 
	 * @param filename xml fájl neve
	 * @return JSONArray amit az xml adataiból készít
	 */
	public static JSONArray getJsonFromXML(String filename) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(filename);
			Node rootNode = doc.getDocumentElement();
			NodeList rootList = doc.getElementsByTagName(Constants.CAT_ELEMENT);
			JSONArray jsonArray = new JSONArray();

			for (int i = 0; i < rootList.getLength(); i++) {
				JSONObject jsonObject = new JSONObject();
				printNodeRecursively(rootList.item(i), jsonObject);
				jsonArray.put(jsonObject);
			}
			return jsonArray;
		} catch (SAXException e) {
			System.out.println("File is not acceptable.");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Something happened.");
		}
		return null;
	}

	/**
	 * Rekúrzívan halad végig az xml dokumentumban és ha elér egy text típúsú
	 * node-hoz, eltárolja azt egy paraméterként kapott JSONObject-ben.
	 * 
	 * @param node       aktuális node az xml dokumentumban
	 * @param jsonObject ebbe az objektumba fogja gyűjteni a elemet az értékkel
	 */
	private static void printNodeRecursively(Node node, JSONObject jsonObject) {

		if (node.hasChildNodes()) {
			NodeList nodeList = node.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node childrenNode = nodeList.item(i);
				if (childrenNode.getNodeType() == Node.ELEMENT_NODE) {
					if (childrenNode.hasChildNodes()) {
						printNodeRecursively(childrenNode, jsonObject);
					}
				} else if (childrenNode.getNodeType() == Node.TEXT_NODE) {
					String text = childrenNode.getTextContent();
					if (text.trim().length() > 0) {
						jsonObject.put(node.getNodeName(), childrenNode.getTextContent());
					}
				}
			}
		}
	}

	/**
	 * Xml fájlt készít a paraméterként kapott kollekcióból a project jegyzékbe,
	 * "database.xml" néven.
	 * 
	 * @param collection amelyet az xml fájlba ír
	 */
	public static void writeCollectionToXml(MongoCollection<org.bson.Document> collection) {

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document XmlDocument = dBuilder.newDocument();

			// root element (cats)
			Element rootElement = XmlDocument.createElement(Constants.COLLECTION_NAME);
			XmlDocument.appendChild(rootElement);

			for (org.bson.Document documentFromCollection : collection.find()) {
				// cat element
				Element cat = XmlDocument.createElement(Constants.CAT_ELEMENT);
				rootElement.appendChild(cat);

				// cat id attribute
				if (documentFromCollection.containsKey(Constants.MONGODB_ID)) {
					Attr attrType = XmlDocument.createAttribute(Constants.MONGODB_ID);
					attrType.setValue(documentFromCollection.get(Constants.MONGODB_ID).toString());
					cat.setAttributeNode(attrType);
				}

				// name element for a cat
				if (documentFromCollection.containsKey(Constants.CAT_NAME_ELEMENT)) {
					Element name = XmlDocument.createElement(Constants.CAT_NAME_ELEMENT);
					name.appendChild(
						XmlDocument.createTextNode((String) documentFromCollection.get(Constants.CAT_NAME_ELEMENT)));
					cat.appendChild(name);
				}

				// dateborn element for a cat
				if (documentFromCollection.containsKey(Constants.CAT_DATE_OF_BIRTH_ELEMENT)) {
					Element dateborn = XmlDocument.createElement(Constants.CAT_DATE_OF_BIRTH_ELEMENT);
					dateborn.appendChild(XmlDocument
						.createTextNode((String) documentFromCollection.get(Constants.CAT_DATE_OF_BIRTH_ELEMENT)));
					cat.appendChild(dateborn);
				}

				// gender element for a cat
				if (documentFromCollection.containsKey(Constants.CAT_GENDER_ELEMENT)) {
					Element gender = XmlDocument.createElement(Constants.CAT_GENDER_ELEMENT);
					gender.appendChild(
						XmlDocument.createTextNode((String) documentFromCollection.get(Constants.CAT_GENDER_ELEMENT)));
					cat.appendChild(gender);
				}
				// favorite_food element for a cat
				if (documentFromCollection.containsKey(Constants.CAT_FOOD_ELEMENT)) {
					Element favoriteFood = XmlDocument.createElement(Constants.CAT_FOOD_ELEMENT);
					favoriteFood.appendChild(
						XmlDocument.createTextNode((String) documentFromCollection.get(Constants.CAT_FOOD_ELEMENT)));
					cat.appendChild(favoriteFood);
				}

			}
			// write the content into xml file
			XmlDocument.setXmlStandalone(true);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(XmlDocument);
			StreamResult result = new StreamResult(new File("database.xml"));
			transformer.transform(source, result);

			// Output to console for testing
			System.out.println("Database saved to database.xml in the project's root directory");
			StreamResult consoleResult = new StreamResult(System.out);
			transformer.transform(source, consoleResult);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
