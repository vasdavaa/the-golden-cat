package hu.miskolc.uni.msc.modern.db.utils;

import java.util.Iterator;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Updates;

/**
 * Mongodb-hez kapcsolódó hasznos függvények könyvtára.
 * 
 * @author VassDavidAttila
 * @neptunCode F92356
 */
public class MongoDbUtilities {

	/**
	 * Visszaadja a cats nevű kollekciót a TheGoldenCats nevű adatbázisból.
	 * Mivel a mongodb alapértelmezetten a 27017 portot használja mint szolgáltatás,
	 * ezért ezen próbálkozok én is. (Nem fog működni a program amíg át nem
	 * helyeztük erre a portra, de ezt még átalakíthatom ha szükséges)
	 * Létrehozom az adatbázist és a kollekciót. A mongodb létrehozza az adatbázist
	 * ha még nem létezett, azonban a kollekció létrehozásnál viszont hibát dobna ha
	 * nem ellenőrizném.
	 * 
	 * @return cats nevű kollekció a TheGoldenCats nevű adatbázisból
	 */
	public static MongoCollection<Document> getCollection() {
		// Default MongoDB Port is 27017
		System.out.println("Searching for mongodb service on port 27017...");
		MongoClient mongoClient = new MongoClient("localhost", 27017);

		try {
			// If database is not present, MongoDB will create it.
			MongoDatabase database = mongoClient.getDatabase(Constants.DATABASE_NAME);
			if (!checkIfCollectionExist(database)) {
				database.createCollection(Constants.COLLECTION_NAME);
				System.out.println("Collection created");
			}
			MongoCollection<Document> collection = database.getCollection(Constants.COLLECTION_NAME);
			return collection;
		} catch (MongoException e) {
			System.out.println("Not found...");
			System.exit(0);
		}
		return null;
	}

	/**
	 * Paraméterként kapott kollekcióba beszúrja a json String-et, miután készített
	 * egy dokumentumot belőle.
	 * 
	 * @param collection A kollekció amibe a json Stringet fogja elhelyezni.
	 * @param json       A szöveg amit el fog helyezni a kollekcióban.
	 */
	public static void insertJsonIntoCollection(MongoCollection<Document> collection, String json) {
		JSONObject jo = new JSONObject(json);
		System.out.println(jo.toString() + " inserted into the database.");
		Document document = new Document().parse(json);
		collection.insertOne(document);
	}

	/**
	 * Megnézi, hogy létezik-e a megadott adatbázisban a "cats" kollekció.
	 * 
	 * @param database az adatbázisban amiben ellenörzöm a kollekció létezését.
	 * @return true ha létezik, false egyébként
	 */
	private static boolean checkIfCollectionExist(MongoDatabase database) {
		Iterator iter = database.listCollectionNames().iterator();
		while (iter.hasNext()) {
			if (Constants.COLLECTION_NAME.equals(((String) iter.next()))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Kiírja egy paraméterként kapott kollekció adatait a console-ra.
	 * 
	 * @param collection kollekció aminek az adatai kiírásra kerülnek
	 */
	public static void printCollection(MongoCollection<Document> collection) {
		int id = 0;
		for (Document doc : collection.find()) {
			System.out.println("id: [" + id + "] Cat");
			System.out.println("Name: " + doc.get(Constants.CAT_NAME_ELEMENT)
				+ "\nDate of birth: " + doc.get(Constants.CAT_DATE_OF_BIRTH_ELEMENT)
				+ "\nGender: " + doc.get(Constants.CAT_GENDER_ELEMENT)
				+ "\nFavorite food: " + doc.get(Constants.CAT_FOOD_ELEMENT));
			System.out.println();
			id++;
		}
	}

	/**
	 * Visszadja a paraméterként kapott kollekció sorrendszeri ObjectId-ját.
	 * 
	 * @param collection amelyben keresi az adott indexű objectId-t
	 * @param index      a kollekció alapértelmezett sorrendjének adott indexe
	 * @return ObjectId a kollekció alapértelmezett sorrendjében adott indexű elem
	 *         ObjectId-ja
	 */
	public static ObjectId getDocumentIdFromCollection(MongoCollection<Document> collection, int index) {
		int id = 0;
		for (Document doc : collection.find()) {
			if (index == id) {
				return doc.getObjectId("_id");
			}
			id++;
		}
		return null;
	}

	/**
	 * Paraméterként kapott azonosító filter alapján talált elemet kitörli a
	 * kollekcióból.
	 * 
	 * @param filterForId azonosító filter ( _id="abcdef12345" )
	 * @param collection  amelyből törlődik a dokumentum
	 */
	public static void deleteObjFromCollection(Bson filterForId, MongoCollection<Document> collection) {
		collection.deleteMany(filterForId);
	}

	/**
	 * Paraméterként kapott kollekcióban szereplő dokumentumok törlése, üres
	 * dokumentummal(ami mindenre találatot ad).
	 * 
	 * @param collection amelyből törlődnek a dokumentumok
	 */
	public static void deleteCollectionFromDatabase(MongoCollection<Document> collection) {
		BasicDBObject document = new BasicDBObject();
		collection.deleteMany(document);
	}

	/**
	 * Adott kollekcióban adott _id val rendelkező dokumentum, adott adattagjának
	 * törlése.
	 * 
	 * @param filterForId azonosító filter ( _id="abcdef12345" )
	 * @param fieldName   az adattag amely törlésre kerül
	 * @param collection  adott kollekció amelyben elvégezzük az adattag törlést
	 */
	public static void deleteDocumentField(Bson filterForId, String fieldName, MongoCollection<Document> collection) {
		collection.updateMany(filterForId, Updates.unset(fieldName));
	}

	/**
	 * Adott kollekcióban egy dokumentum, adott dokumentumok, összes adattagjának
	 * felülírása, adott értékekkel.
	 * 
	 * @param filterForId      azonosító filter ( _id="abcdef12345" )
	 * @param fieldName        az adattaggok amelyek módosításra kerülnek
	 * @param newValueForField az adattaggok új értékei amivel felülírjuk őket
	 * @param collection       adott kollekció amiben végezzük a módosításokat
	 */
	public static void updateDocumentFields(Bson filterForId, String[] fieldName, String[] newValueForField, MongoCollection<Document> collection) {
		for (int i = 0; i < fieldName.length; i++) {
			collection.updateMany(filterForId, Updates.set(fieldName[i], newValueForField[i]));
		}
	}

	/**
	 * * Adott kollekcióban egy dokumentum, adott dokumentumok, egy adattagjának
	 * felülírása, adott értékkel.
	 * 
	 * @param filterForId      azonosító filter ( _id="abcdef12345" )
	 * @param fieldName        az adattag amely módosításra kerül
	 * @param newValueForField az adattag új értéke amivel felülírjuk
	 * @param collection       adott kollekció amiben végezzük a módosítást
	 */
	public static void updateDocumentField(Bson filterForId, String fieldName, String newValueForField, MongoCollection<Document> collection) {
		collection.updateMany(filterForId, Updates.set(fieldName, newValueForField));
	}

}
